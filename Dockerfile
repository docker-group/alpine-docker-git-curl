FROM docker:git

RUN apk add --no-cache \
              curl \
              bash \
              which \
              python \
              wget
              
RUN curl -sSL https://sdk.cloud.google.com | bash

ENV PATH $PATH:/root/google-cloud-sdk/bin